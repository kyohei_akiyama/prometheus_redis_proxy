# -*- coding: utf-8 -*-

import os
import re

from nose.tools import ok_
from webtest import TestApp
from app import wsgi_app

RULES = [
    (re.compile(r'^redis_up \d$'), 1,),
    (re.compile(r'^redis_uptime_seconds \d+$'), 1,),
    (re.compile(r'^redis_cpu_status\{.*\} [\d.]+$'), 4,),
    (re.compile(r'^redis_general_status\{.*\} \d+$'), 9,),
    (re.compile(r'^redis_memory_status\{.*\} [\d.]+$'), 6,),
    (re.compile(r'^redis_clients_status\{.*\} \d+$'), 2,),
]


def test_get_redis_metrics():
    url = '/metrics?host=%s&port=%s' % (
        os.environ['REDIS_HOST'], os.environ['REDIS_PORT'],
    )
    app = TestApp(wsgi_app)
    res = app.get(url)

    ok_(res.status_int == 200)
    stats = res.body.decode('utf-8').split('\n')
    for rule, line_num in RULES:
        ok_(len([s for s in stats if rule.match(s)]) == line_num)

    r = re.compile(r'^redis_key_space_status\{.*\} \d+$')
    ok_(len([s for s in stats if r.match(s)]) >= 2)
