# -*- coding: utf-8 -*-

import os
import sys
import traceback
import re
import redis
from bottle import Bottle, run, request, response


NAME_PREFIX = os.getenv('NAME_PREFIX', default='redis')
INFO_KEYS = {
    'clients_status': [
        'connected_clients',
        'blocked_clients',
    ],
    'memory_status': [
        'used_memory',
        'used_memory_rss',
        'used_memory_peak',
        'total_system_memory',
        'maxmemory',
        'mem_fragmentation_ratio',
    ],
    'general_status': [
        'total_connections_received',
        'total_commands_processed',
        'rejected_connections',
        'expired_keys',
        'evicted_keys',
        'keyspace_hits',
        'keyspace_misses',
        'pubsub_channels',
        'pubsub_patterns',
    ],
    'cpu_status': [
        'used_cpu_sys',
        'used_cpu_user',
        'used_cpu_sys_children',
        'used_cpu_user_children',
    ]
}


def get_redis_conn(conn_args):
    return redis.StrictRedis(**conn_args)


def get_up(up=True):
    if up:
        return ['%s_up 1' % (NAME_PREFIX)]
    else:
        return ['%s_up 0' % (NAME_PREFIX)]


def get_stats(conn):
    return conn.info()


def convert_system_stats(stats):
    ret = []

    # Uptime
    ret.append('%s_uptime_seconds %s' %
               (NAME_PREFIX, stats['uptime_in_seconds']))

    for section, labels in INFO_KEYS.items():
        for label in sorted(labels):
            value = stats.get(label, None)
            if value is not None:
                ret.append('%s_%s{name="%s"} %s' %
                           (NAME_PREFIX, section, label, value))
    return ret


def convert_key_space_stats(stats):
    ret = []
    r = re.compile(r'^db\d+$')
    for db, stat in [(db, stat,) for db, stat in stats.items() if r.match(db)]:
        ret.append('%s_key_space_status{db="%s",name="keys"} %s' %
                   (NAME_PREFIX, db, stat['keys']))
        ret.append('%s_key_space_status{db="%s",name="expires"} %s' %
                   (NAME_PREFIX, db, stat['expires']))
    return ret


wsgi_app = Bottle()


@wsgi_app.get('/metrics')
def get_db_metrics():
    try:
        conn_args = {
            'db': 0,
            'host': request.query['host'],
            'port': int(request.query['port']),
            'socket_timeout': 1
        }
        ret = []
        try:
            conn = get_redis_conn(conn_args)
            stats = get_stats(conn)
            ret += get_up(up=True)
            ret += convert_system_stats(stats)
            ret += convert_key_space_stats(stats)
        except Exception as e:
            ret += get_up(up=False)

        response.content_type = 'text/plain'
        return '\n'.join(ret) + '\n'
    except Exception as e:
        response.status = 500
        st = traceback.format_exc()
        sys.stderr.write(st)
        return st


if __name__ == '__main__':
    run(wsgi_app, host='localhost', port=8080)
