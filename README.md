## Installing dependent packages
```shell
pip install bottle redis
```

or

```shell
pip install -r requirements.txt
```

## Running
```shell
python app.py
```

## Running with Gunicorn
```shell
gunicorn -b 0.0.0.0:8000 app:wsgi_app
```

## Usage
```shell
$ curl 'http://127.0.0.1:8000/metrics?host=127.0.0.1&port=6379'
redis_general_status{name="evicted_keys"} 0
redis_general_status{name="expired_keys"} 3
redis_general_status{name="keyspace_hits"} 5
redis_general_status{name="keyspace_misses"} 0
redis_general_status{name="pubsub_channels"} 0
redis_general_status{name="pubsub_patterns"} 0
redis_general_status{name="rejected_connections"} 0
redis_general_status{name="total_commands_processed"} 61
redis_general_status{name="total_connections_received"} 61
redis_cpu_status{name="used_cpu_sys"} 170.24
redis_cpu_status{name="used_cpu_sys_children"} 0.0
redis_cpu_status{name="used_cpu_user"} 49.12
redis_cpu_status{name="used_cpu_user_children"} 0.0
redis_memory_status{name="maxmemory"} 0
redis_memory_status{name="mem_fragmentation_ratio"} 4.93
redis_memory_status{name="total_system_memory"} 2096275456
redis_memory_status{name="used_memory"} 827608
redis_memory_status{name="used_memory_peak"} 828632
redis_memory_status{name="used_memory_rss"} 4083712
redis_clients_status{name="blocked_clients"} 0
redis_clients_status{name="connected_clients"} 1
```

## Test
```shell
REDIS_HOST=127.0.0.1 REDIS_PORT=6379 nosetests -sv test_app.py
```

or

```shell
REDIS_HOST=127.0.0.1 REDIS_PORT=6379 tox
```
