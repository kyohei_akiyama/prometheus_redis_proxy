FROM python:3.6

ADD requirements.txt /
RUN pip install -r /requirements.txt

ADD app.py /

WORKDIR /

CMD ["gunicorn", "-b", "0.0.0.0:8000", "--threads", "16", "app:wsgi_app"]
